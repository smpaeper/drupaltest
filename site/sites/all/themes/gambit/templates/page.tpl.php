<div id="site-header">
        <div id="site-header-image">
            <img src="./<?php print path_to_theme(); ?>/imgs/banner.jpg" title="Home" alt="Ivory Trunks" />
        </div>
        <div class="container top">
            <div id="logo">
              <a href="/">
                <img src="./<?php print path_to_theme(); ?>/imgs/logo.png" />
              </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="menu-nav-container main">
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
    </div>
    <div class="container content">
        <div class="clearfix">
            
            <h1><?php print $title; ?></h1>

            <?php if ($messages): ?>
                <div id="messages"><div class="section clearfix">
                  <?php print $messages; ?>
                </div></div> <!-- /.section, /#messages -->
            <?php endif; ?>
            
            <?php if ($tabs): ?>
                <div class="tabs">
                    <?php print render($tabs); ?>
                </div>
            <?php endif; ?>

            <?php if($page['left_callout']): ?>
                <div class="<?php print $variables['gambit_widthClasses']['column'] ?>">
                    <?php print render($page['left_callout']); ?>
                </div>
            <?php endif; ?>

            <div id="above_content">
                    <?php if($page['above_content']): ?>
                        <div>
                            <?php print render($page['above_content']); ?>
                        </div>
                    <?php endif; ?>
            </div>

            <div class="<?php print $variables['gambit_widthClasses']['main'] ?>">
                <?php
                    print render($page['content']);
                ?>
            </div>
            
            <?php if($page['right_callout']): ?>
                <div class="<?php print $variables['gambit_widthClasses']['column'] ?>">
                    <?php print render($page['right_callout']); ?>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
    
    <div id="pre-footer">
            <?php if($page['pre_footer']): ?>
                <div>
                    <?php print render($page['pre_footer']); ?>
                </div>
            <?php endif; ?>
    </div>
    
    <div id="site-footer">
        <div class="container green">
            <div class="menu-nav-container footer">
                <?php 
                    print theme('links',array('links'=>$main_menu));
                ?>              
                <div id="footer_text">
                    <?php if($page['footer_text']): ?>
                        <div>
                            <?php print render($page['footer_text']); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
