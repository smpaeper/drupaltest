    <div class="site-container">
        
        <div class="gradient-overlay">
        </div>
        
        <!-- header -->
        <div class="header-wrapper">
        </div>
        
        <!-- site title -->
        <div class="site-title-container">
            <div class="site-title">
                <p>Photography Blog</p>
            </div>
        </div>
        
        <!-- content -->
        <div class="content-container container">
            <div class="menu-container inner-container">
            	<?php print theme('links',array('links'=>$main_menu));?>
            </div>
            
            <div class="tab-container container">
            	<!-- tabs will go here -->
            	<?php if ($tabs): ?>
            	    <div class="tabs">
            	        <?php print render($tabs); ?>
            	    </div>
            	<?php endif; ?>
            </div>
            
            <div class="title inner-container">
            	<h1><?php print $title; ?></h1>
            </div>
            
            <div class="content inner-container clearfix">
                <div class="main-content three-fourths left">
                    <div class="content">
                        <!-- messages will go here -->
            			<?php if ($messages): ?>
                			<div id="messages">
                            	<div class="section clearfix">
                					<?php print $messages; ?>
                				</div>
                   			</div> <!-- /.section, /#messages -->
            			<?php endif; ?>

                		<?php print render($page['content']);?>
                    </div>
                </div>
                
                <div class="right-column column region one-fourth left">
                	<?php print render($page['right_callout']); ?>
                </div>
            </div>
        </div> 	<!-- end content -->
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
            	<?php print render($page['footer']); ?>
            </div>
        </div>
        
    </div>
